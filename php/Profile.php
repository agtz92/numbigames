<?php session_start()?>

<!DOCTYPE html5>

<html>
    <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/> <!--Import materialize.css                          -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>                            <!--Let browser know website is optimized for mobile-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />                              <!--Allow special characters                        -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">             <!--To be able to use Materialize icons             -->
    <head>
        <style>
        #div1 {width:50px;height:50px;padding:10px;border:1px solid #aaaaaa;}
        </style>
        <title id="tops">Numbi All-Games</title>
    </head>
    <body>  
        <header>
        <!-- Navbar goes here --><!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content">
                <li class="divider"></li><li><a href="../game1.html">Run Like Hell</a></li>
                <li class="divider"></li><li><a href="../game2.html">Virtual Pet Monster</a></li>
                <li class="divider"></li><li><a href="../game3.html">Glow Tennis</a></li>
                <li class="divider"></li><li><a href="../game4.html">Swing Alien</a></li>
            </ul>
            <ul id="dropdown2" class="dropdown-content">
                <li class="divider"></li><li><a href="../Subscribe_Developer.html">Developer Account</a></li>
                <li class="divider"></li><li><a href="../Subscribe_Gamer.html">Gamer Account</a></li>
            </ul>
            
            <nav>
                <div class="nav-wrapper">
                <img href="../index.html" width=95px height=60px src="../images/vControler.png"> 
                <a href="../index.html" class="brand-logo">NumbiGames</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="../Log_In.html">Log in to your account</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Subscribe to NumbiGames!<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">AvailableGames - PLAY!!!<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="../about.html">AboutUs</a></li>
                </ul>
                </div>
            </nav>
        </header>
        <body>
            
            
            <div class="row">
                
                <div class="col s12 m12">
                    <div class="col s12 m12">
                      <h2 class="h52 ">Upload Profile Pic</h2>
                      
                      <div class="row">
                      <div class="icon-block center">
                        <?php 
                        
                        echo "<img width='300' height='300' src='../images/".$_SESSION['ppic']."' alt='Profile Pic'>";
                        
                        ?>
                      </div>
                      </div>
                      
                      <div class="icon-block">
                        
                        
                        <!--<h2 class="center grey-text text-accent-4"><i class="large material-icons">account_circle</i></h2>-->
                        
                          <form  action="uploadPhoto.php" method="post" enctype="multipart/form-data">
                            
                          <div class="file-field input-field">
                            
                            <div class="btn yellow black-text bold-text">
                              
                              <span>Browse Pic</span>
                              <input type="file" name="fileToUpload" id="fileToUpload">
                              <input class="file-path validate white-text" type="text" name="newppic">
                              
                            </div>
                          <button class="btn yellow black-text bold-text" type="submit" name="submit">Upload Pic
                                 
                                  <i class="material-icons right">send</i>
                          </button> 
                          
                          </div>
                          
                        </form>
                        
                      </div>
                      
                      
                      
                      
                      
                    </div>
                </div>
                <br><br>
                
            </div>
            <div class="row">
                <div class="col s12 m12">
                  
                 <h2 class="center red-text tex-accent-4">Your account:</h2> 
                  
                </div>
            </div>
            
            
            <div class="row">
              
                <div class="col s12">

                    <div class="divider"></div>
                      <div class="section">
                         
                        <h5>Your Numbi's username is: <strong><?php echo $_SESSION["u"] ?></strong></h5>
                        <!--<p>Variable de base de datos</p>-->
                      </div>
                    <div class="divider"></div>
                      <div class="section">
                        <h5>Your e-mail account is:  <?php echo $_SESSION["m"] ?></h5>
                        <!--<p>Variable de base de datos</p>-->
                      </div>
                      
                      <div class="divider"></div>
                      
                      <div class="section">
                         <h5>Your Profile Pic is:  <?php echo $_SESSION["ppic"] ?></h5>
                        <!--<p>Variable de base de datos</p>-->
                      </div>
            </div>    
            </div>
            
            
        </body>
        
            <!--Footer Personalizado-->
    <!--<div class=" col s12">-->
    <footer class="page-footer yellow">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="orange-text">About</h5>
              <p class="deep-orange-text text-darken-4">NumbiGames 2015</p>
              <p class="deep-orange-text text-darken-4">JLPZ AGTZ</p>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
     </div>
  </footer>
  <!--</div>-->
 
  
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>         <!--Import jQuery before materialize.js             -->
    <script type="text/javascript" src="../js/materialize.js"></script>
    
</html>
