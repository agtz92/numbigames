<?php

 $host = "127.0.0.1";
    $user = "alek56";                     //Your Cloud 9 username
    $pass = "";                                 //Remember, there is NO password by default!
    $db = "Numbi";                          //Your database name you want to connect to
    $port = 3306;  


if(isset($_POST["usrname"]))
{
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        die();
    }
    $mysqli = new mysqli($host , $user, $pass, $db);
    if ($mysqli->connect_error){
        die('Could not connect to database!');
    }
    
    $username = filter_var($_POST["usrname"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
    
    $statement = $mysqli->prepare("SELECT pseudonym FROM Desarrolladores WHERE pseudonym=?");
    $statement->bind_param('s', $username);
    $statement->execute();
    $statement->bind_result($username);
    if($statement->fetch()){
        die('<img src="../images/Red-Cross-Mark.png" />');
    }else{
        die('<img src="../images/Check_mark.png" />');
    }
}
?>